from typing import Optional
from sqlalchemy.orm import Session

#  give client common keywords to autocomplete search


#  search results

def search_user(searched_word:str, database, max_count:int=20) -> Optional[list]:  # returns user dict list
    with Session(database.engine) as s:
        results = s.query(database.nameToUser).filter(database.nameToUser.name.like(f'%{searched_word}%')).limit(max_count).all()
        if results is None:
            return None
    return [database.getUser(i.user_id) for i in results]
