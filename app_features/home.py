import heapq
import random
from sqlalchemy.orm import Query, Session
import numpy as np
import math


def post_arr(borpa_arr_size:int, n:int) -> list:
    if n >= borpa_arr_size:
        return list(range(borpa_arr_size))
    distortion = 1.4
    random_array = np.random.random(n)*(np.arange(0, n)/n)**distortion*borpa_arr_size
    random_array = random_array.round()
    random_array = random_array.astype("int")
    random_array = np.unique(random_array)
    random_array = np.append(random_array, np.arange(n))
    random_array = np.unique(random_array)
    np.random.shuffle(random_array)
    if random_array.__len__() > n:
        random_array = np.delete(random_array, np.arange(random_array.__len__()-n))
    return list(random_array)


# rückmeldungen zu weights
# wenn man borpa sieht ohne zu liken rechnet man score -0.02 oder so
def looked_at_borpa(database, borpa_id:int):
    database.writeBorpaScore(borpa_id, database.getBorpaScore(borpa_id)-0.02)

# user prefrences detimed (user hat fav games und users + borpas haben eigenes ranking)
def get_home_screen_borpas_logged_in(database, user_id:str, count:int = 20):    # dysfunctional
    s:Session
    query:Query 
    s, query = database.getBorpaScoreSessionQuery()

    query = query.order_by(database.BorpaScore.score.desc())
    positions = post_arr(query.count(), count*6)  # *6 anpassbar

    # objekte kriegen an positions
    # geht bestimmt auch besser
    scores_obj = []
    for i in positions:
        scores_obj.append(query.offset(i).first())

    preferences = database.getUserPreferences(user_id)
    if preferences is None:
        database.writeUserPreferences(user_id, {"games":{}, "users":{}})
        preferences = {"games":{}, "users":{}}

    for i in scores_obj:
        borpa_data = database.read_borpa(i.borpa_id)
        if borpa_data["game"] in preferences["games"].keys():
            i.score += preferences["games"][borpa_data["game"]]
        if borpa_data["user_id"] in preferences["users"].keys():
            i.score += preferences["users"][borpa_data["user_id"]]

    scores_presort = [i.score for i in scores_obj]
    scores = sorted(scores_presort, reverse=True)

    score_to_borpa = {}
    for i in scores_obj:
        if not i.score in score_to_borpa.keys():
            score_to_borpa[i.score] = [i.borpa_id]
        else:
            score_to_borpa[i.score].append(i.borpa_id)

    user = database.read_user(user_id)
    out_arr = []

    for score in score_to_borpa.keys():
        for borpa_id in score_to_borpa[score]:
            borpa = database.read_borpa(borpa_id)
            borpa["liked"] = borpa_id in user["liked_borpas"] 
            out_arr.append(borpa)
        if len(out_arr) >= count:
            break

    s.close()
    return out_arr[:count]

def get_home_screen_borpas_standard(database, count:int=20):
    s:Session
    query:Query 
    s, query = database.getBorpaScoreSessionQuery()

    query = query.order_by(database.BorpaScore.score.desc())
    positions = post_arr(query.count(), count*6)  # *6 anpassbar

    # objekte kriegen an positions
    # geht bestimmt auch besser
    scores_obj = []
    for i in positions:
        scores_obj.append(query.offset(i).first())

    scores_presort = [i.score for i in scores_obj]
    scores = sorted(scores_presort, reverse=True)

    score_to_borpa = {}
    for i in scores_obj:
        if not i.score in score_to_borpa.keys():
            score_to_borpa[i.score] = [i.borpa_id]
        else:
            score_to_borpa[i.score].append(i.borpa_id)

    out_arr = []

    for score in score_to_borpa.keys():
        for borpa_id in score_to_borpa[score]:
            borpa = database.read_borpa(borpa_id)
            borpa["liked"] = False
            out_arr.append(borpa)
        if len(out_arr) >= count:
            break

    s.close()
    return out_arr[:count]
