from game_api_helper import game_const

def getLinkingSite(game:str, user_ID:str="-1"):
    helper = game_const.HELPER_DICT[game] 
    helper_obj = helper(user_ID)
    return helper_obj.get_linking_site()

def get_data(game:str, id:str):  #  in game name
    helper = game_const.HELPER_DICT[game] 
    helper_obj = helper(id)
    try:
        data = helper_obj.get_data()
    except:
        data = game_const.gamedata.IGameData(gameName=game, gameUserID=id, inGameName="An error accured while loading game data").toDict()
    return data
