import json
import os
from pickle import DICT
import time

from fastapi import WebSocket
from util import goodchars, web_socket_messages

from sqlalchemy import DateTime, ForeignKey, Integer, String, create_engine, func, update, Column
from sqlalchemy.orm import Mapped, mapped_column, relationship, DeclarativeBase, Session, Query
from sqlalchemy.dialects.postgresql import JSONB, JSON

from util.double_sided_dict import DoubleSidedDict
engine = create_engine("sqlite:///database.db", echo=False)


with open("settings/standard_values.json", "r") as f:
    standard_settings_values = json.load(f)
max_username_length = 20

class Base(DeclarativeBase):
    pass

from turtle import st
from typing import Any, List, Optional, Tuple

# user database

"""
notes:
- database structure (json)
    + profile info (name, email, passwrd)
    + friends (stored in list)
    + game data (stored in dict game:id)
    + settings
    + borpas (stored in list)
"""

class User(Base):  # vielleicht noch zu preferences linken
    __tablename__ = "user"

    id: Mapped[str] = mapped_column(primary_key=True)
    profile_info: Mapped[Optional[dict[str,str]]]  = mapped_column(JSON(none_as_null=True))
    friends: Mapped[Optional[str]] = mapped_column(JSON)# list
    game_data: Mapped[Optional[dict[str,str]]]  = mapped_column(JSON(none_as_null=True))
    settings: Mapped[Optional[dict[str,Any]]]  = mapped_column(JSON(none_as_null=True))
    borpas: Mapped[Optional[str]] = mapped_column(JSON) # list 
    liked_borpas: Mapped[Optional[str]] = mapped_column(JSON) # list
    # additional info to be added

    def to_dict(self) -> dict:
        return {
            "profile_info": self.profile_info,
            "friends":self.friends,
            "game_data":self.game_data,
            "settings":self.settings,
            "borpas":self.borpas,
            "liked_borpas":self.liked_borpas
        }


def user_profile(id:int=-1, name:str="", email:str="", password:str="", friends:list=[], game_data:dict={}):
    profile_dict = {
        "profile_info": {
            "user_id":id,
            "name":goodchars.removeBadChars(name, goodchars.name),
            "email":email,
            "password":password,
            "profile_picture":"",
            "email_verified":False
        },
        "friends":friends,
        "game_data":game_data,
        "settings":standard_settings_values,
        "borpas":[],
        "liked_borpas":[]
    }
    return profile_dict

def read_user(id:str, safe=False) -> Optional[dict]:
    with Session(engine) as session, session.begin():
        if ((u:=session.query(User).get(str(id)))!=None):
            user_dict = u.to_dict()
            if not hasattr(user_dict["settings"], "keys"): user_dict["settings"] = standard_settings_values
            userSettingsKeys = user_dict["settings"].keys()
            for i in standard_settings_values.keys():
                if i not in userSettingsKeys:
                    user_dict["settings"][i] = standard_settings_values[i]

            if safe:
                user_dict["profile_info"]["password"] = ""
                user_dict["profile_info"]["email"] = ""

            return user_dict
        
def getUser(id:str) -> Optional[dict]:
    return read_user(id)

def getUserProfileInfo(user_id:str ,safe=True):
    with Session(engine) as session, session.begin():
        if ((u:=session.query(User).get(str(user_id)))!=None):
            user_dict = {
                "profile_info":u.profile_info
            }

            if safe:
                user_dict["profile_info"]["password"] = ""
                user_dict["profile_info"]["email"] = ""

            return user_dict

def save_user(profile_dict, id:str) -> None:
    with Session(engine) as session, session.begin():
        session.merge(User(
            id=str(id), 
            profile_info=profile_dict["profile_info"], 
            friends=profile_dict["friends"],
            game_data=profile_dict["game_data"],
            settings=profile_dict["settings"],
            borpas=profile_dict["borpas"],
            liked_borpas=profile_dict["liked_borpas"]
        ))

def del_user(id:int, emailConfirm:bool = False) -> None:  # ???
    with Session(engine) as session, session.begin():
        alreadyMarkedObj:UserMarkedForDeletion = session.query(UserMarkedForDeletion).get(id)
        if alreadyMarkedObj is not None:  
            if alreadyMarkedObj.date+60 < time.time() or emailConfirm: # one hour passed then delete immediatly
                for borpa in session.query(Borpa).filter(Borpa.user_id==id):
                    del_borpa(borpa.id)
                save_user(user_profile(id=id, name="deleted user"), id)
                session.delete(session.query(emailToUser).filter(emailToUser.user_id==id).first())
                session.delete(session.query(nameToUser).filter(nameToUser.user_id==id).first())
                session.delete(session.query(loginSessionToUser).filter(loginSessionToUser.user_id==id).first())

                Comment.__table__.delete().where(Comment.author_id==id)
                Subcomment.__table__.delete().where(Subcomment.author_id==id)

                return True
        else:
            session.add(UserMarkedForDeletion(
                user_id = id,
                date = time.time()
            ))
            return False
        
def areFriends(user_id1, user_id2):
    pass

# borpa database

"""
notes:
- borpa structure (json)
    + user id (author)
    + game
    + body (archievement(rampage, penta, in game hours, etc.))
    + text
    + linked media (only one)
"""

class Borpa(Base): # vielleicht noch zu score linken
    __tablename__ = "borpa"

    id: Mapped[str] = mapped_column(primary_key=True)
    user_id: Mapped[str]
    game: Mapped[str] = mapped_column(String(100))
    body: Mapped[str] = mapped_column(String(1000))
    text: Mapped[str] = mapped_column(String(280))
    linked_media: Mapped[Optional[str]]
    likes: Mapped[int]
    date: Mapped[float]

    def to_dict(self) -> dict:
        return {
            "borpa_id":self.id,
            "user_id":self.user_id,
            "game":self.game,
            "body":self.body,
            "text":self.text,
            "linked_media":self.linked_media,
            "comments":[],
            "likes":self.likes,
            "date":self.date
        }

def borpa(id:int, user_id:int=-1, game:str="", body:str="", text:str="", linked_media:str=""):
    borpa_dict = {
        "borpa_id":id,
        "user_id":user_id,
        "game":game,
        "body":body,
        "text":text,
        "linked_media":linked_media,
        "comments":[],
        "likes":0,
        "date":time.time()
    }
    return borpa_dict

def read_borpa(id:str, getComments:bool=False) -> dict:
    with Session(engine) as session, session.begin():
        if ((b:=session.query(Borpa).get(id))!=None):
            borpa_dict = b.to_dict()
            if getComments:
                borpa_dict["comments"] = session.query(Comment).filter(Comment.linked_borpa_id==id)[:10]
                borpa_dict["comments"] = [i.to_dict() for i in borpa_dict["comments"]]
                for i in borpa_dict["comments"]:
                    user_profile_data = session.query(User).get(i["author_id"])
                    i["author_name"] = user_profile_data.profile_info["name"] 
                    i["author_profile_picture"] = user_profile_data.profile_info["profile_picture"] 
            return borpa_dict

def save_borpa(borpa_dict) -> None:
    with Session(engine) as s, s.begin():
        s.merge(Borpa(
            id=borpa_dict["borpa_id"],
            user_id=borpa_dict["user_id"],
            game=borpa_dict["game"],
            body=borpa_dict["body"],
            text=borpa_dict["text"],
            linked_media=borpa_dict["linked_media"],
            likes=borpa_dict["likes"],
            date=borpa_dict["date"]
        ))

def link_borpa_to_user(user_id:int, borpa_id:int):
    user_data = read_user(user_id)
    borpa_data = read_borpa(borpa_id)
    user_data["borpas"].append(borpa_id)
    borpa_data["user_id"] = user_id
    save_borpa(borpa_data)
    save_user(user_data, user_id)

def del_borpa(id:str) -> None:  # work in progress
    with Session(engine) as s:
        borpa_obj : Borpa = s.query(Borpa).get(id)
        if borpa_obj is None: return #  borpa not existant
        borpa_obj.game = ""
        borpa_obj.body = ""
        borpa_obj.text = "deleted"
        borpa_obj.linked_media = ""
        borpa_obj.user_id = "-1"
        borpa_score_obj = s.query(BorpaScore).get(id)
        if borpa_score_obj is None: return #  borpa score not existant
        borpa_score_obj.score = -100

        try:
            user_data:User = s.query(User).get(borpa_obj.user_id)
            borpas = user_data.borpas.copy()
            borpas.remove(int(id))
            user_data.borpas = borpas
        except:  # does not really matter
            pass

        s.commit()

# comments
class Comment(Base):
    __tablename__ = "comment6"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    linked_borpa_id : Mapped[str] 
    text: Mapped[str] = mapped_column(String(280))
    author_id: Mapped[str]
    subcomment_ids: Mapped[Optional[str]] = mapped_column(JSON) # list
    date: Mapped[float]
    liking_users: Mapped[Optional[str]] = mapped_column(JSON)
    likes: Mapped[int]

    def to_dict(self):
        return {
            "id":self.id,
            "borpa_id":self.linked_borpa_id,
            "text":self.text,
            "author_id":self.author_id,
            "date":self.date,
            "liking_users":self.liking_users,
            "likes":self.likes
        }

def addComment(borpa_id:str, text:str, author_id:str) -> None:
    with Session(engine) as s, s.begin():
        s.add(Comment(
            linked_borpa_id = borpa_id,
            text = text,
            author_id = author_id,
            subcomment_ids = [],
            date = time.time(),
            liking_users = [],
            likes = 0
        ))

def getComment(comment_id:int) -> dict:
    with Session(engine) as s, s.begin():
        comment_obj = s.query(Comment).get(comment_id)
        if comment_obj is not None:
            return comment_obj.to_dict()
        
def likeComment(comment_id:int, liking_author_id:str) -> bool:
    with Session(engine) as s, s.begin():
        comment_obj = s.query(Comment).get(comment_id)
        if comment_obj is not None:
            if liking_author_id in comment_obj.liking_users: return False
            liking_users_temp = comment_obj.liking_users.copy()
            liking_users_temp.append(liking_author_id)
            comment_obj.liking_users = liking_users_temp
            comment_obj.likes = 1 + comment_obj.likes
            return True
        return False
    
def removeLikeComment(comment_id:int, liking_author_id:str) -> bool:
    with Session(engine) as s, s.begin():
        comment_obj = s.query(Comment).get(comment_id)
        if comment_obj is not None:
            if liking_author_id not in comment_obj.liking_users: return False
            liking_users_temp = comment_obj.liking_users.copy()
            liking_users_temp.remove(liking_author_id)
            comment_obj.liking_users = liking_users_temp
            comment_obj.likes = -1 + comment_obj.likes
            return True
        return False

class Subcomment(Base):
    __tablename__ = "subcomment3"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    linked_comment_id : Mapped[str] 
    text: Mapped[str] = mapped_column(String(280))
    author_id: Mapped[str]
    date: Mapped[float]
    liking_users: Mapped[Optional[str]] = mapped_column(JSON)
    likes: Mapped[int]

    def to_dict(self):
        return {
            "id":self.id,
            "comment_id":self.linked_comment_id,
            "text":self.text,
            "author_id":self.author_id,
            "date":self.date,
            "liking_users":self.liking_users,
            "likes":self.likes
        }

def addSubcomment(comment_id:str, text:str, author_id:str) -> None:
    with Session(engine) as s, s.begin():
        s.add(Subcomment(
            linked_comment_id = comment_id,
            text = text,
            author_id = author_id,
            date = time.time(),
            liking_users = [],
            likes = 0
        ))

def getSubcomments(comment_id:str, offset:int=0, num_subcomments:int=50) -> List[dict]:
    with Session(engine) as s, s.begin():
        subcomments = s.query(Subcomment).filter(Subcomment.linked_comment_id==comment_id).offset(offset).limit(num_subcomments).all()
        return [i.to_dict() for i in subcomments]
    
def likeSubcomment(comment_id:int, liking_author_id:str) -> bool:
    with Session(engine) as s, s.begin():
        comment_obj = s.query(Subcomment).get(comment_id)
        if comment_obj is not None:
            if liking_author_id in comment_obj.liking_users: return False
            liking_users_temp = comment_obj.liking_users.copy()
            liking_users_temp.append(liking_author_id)
            comment_obj.liking_users = liking_users_temp
            comment_obj.likes = 1 + comment_obj.likes
            return True
        return False
    
def removeLikeSubcomment(comment_id:int, liking_author_id:str) -> bool:
    with Session(engine) as s, s.begin():
        comment_obj = s.query(Subcomment).get(comment_id)
        if comment_obj is not None:
            if liking_author_id not in comment_obj.liking_users: return False
            liking_users_temp = comment_obj.liking_users.copy()
            liking_users_temp.remove(liking_author_id)
            comment_obj.liking_users = liking_users_temp
            comment_obj.likes = -1 + comment_obj.likes
            return True
        return False

# emailuserregister

class emailToUser(Base):  # machen dass es direkt auf den user zeigt
    __tablename__ = "eu_reg"

    email: Mapped[str] = mapped_column(primary_key=True)
    user_id: Mapped[str]

def getUserIDFromEmail(email:str) -> str:
    with Session(engine) as s, s.begin():
        if (a:=s.query(emailToUser).get(email)) is not None:
            return a.user_id

def writeUserIDFromEmail(email:str, user_id:str) -> str:
    with Session(engine) as s, s.begin():
        s.merge(emailToUser(
            email = email,
            user_id = user_id
        ))

def changeEmail(email:str, user_id:str) -> bool:
    with Session(engine) as s, s.begin():
        if (s.query(emailToUser).get(email) is not None): return False # email taken
        oldEmailObj = s.query(emailToUser).filter(emailToUser.user_id == user_id).first()
        s.delete(oldEmailObj)
        s.merge(emailToUser(
            email = email,
            user_id = user_id
        ))
        return True

#  nameuserregister
#  ein name hat eine list mit ids zugeordnet, damit mehrere leute einen namen haben können

class nameToUser(Base):  # machen dass es direkt auf den user zeigt
    __tablename__ = "nu_reg"

    entry_id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    name: Mapped[str]
    user_id: Mapped[str]

def writeUserIDFromName(user_id:str,name:str) -> None:
    with Session(engine) as s, s.begin():
        s.merge(nameToUser(
            name=name, user_id=user_id
        ))

def updateUserIDFromName(user_id:str, old_name:str, new_name:str) -> None:
    with Session(engine) as s, s.begin():
        s.query(nameToUser).filter(nameToUser.user_id==user_id).update({nameToUser.name: new_name})

def getUserIDsFromName(name:str) -> list[str]:
    with Session(engine) as s, s.begin():
        return [i.user_id for i in s.query(nameToUser).filter(nameToUser.name==name).all()]

# login ids
# user_id : login_session_id

class loginSessionToUser(Base):
    __tablename__ = "loginsession_reg"

    login_session_id: Mapped[str] = mapped_column(primary_key=True) 
    user_id: Mapped[str]
    creation_date: Mapped[int] = mapped_column(DateTime(timezone=False), server_default=func.now())

def writeUserIDFromLoginSession(user_id:str, login_session_id:str) -> None:
    with Session(engine) as s, s.begin():
        s.merge(loginSessionToUser(
            login_session_id=login_session_id,
            user_id=user_id
        ))

def get_current_user(request) -> Optional[str]:
    try:
        login_session_id_cookie = request.headers["login_session_id"]
    except:
        print("login session id missing")
        return
    with Session(engine) as s, s.begin():
        if (a:=s.query(loginSessionToUser).get(login_session_id_cookie)) is not None:
            return a.user_id
        
def get_current_user_by_session_id(login_session_id) -> Optional[str]:
    with Session(engine) as s, s.begin():
        if (a:=s.query(loginSessionToUser).get(login_session_id)) is not None:
            return a.user_id

# //////////////
# home screen
# /////////////

# user preferences database
# user_id : {
#   games: {game:weight}
#   users: {user_id:weight}
# }

class UserPreferences(Base):
    __tablename__ = "userpreferences"

    user_id: Mapped[str] = mapped_column(primary_key=True)
    games: Mapped[Optional[dict[str,str]]]  = mapped_column(JSON())
    users: Mapped[Optional[dict[str,str]]]  = mapped_column(JSON())

    def to_dict(self) -> dict:
        return {
            "games":self.games,
            "users":self.users
        }

def writeUserPreferences(user_id:str, preferences_dict:dict) -> None:
    with Session(engine) as s, s.begin():
        s.merge(UserPreferences(
            user_id = user_id,
            games = preferences_dict["games"],
            users = preferences_dict["users"]
        ))

def getUserPreferences(user_id:str) -> dict:
    with Session(engine) as s, s.begin():
        if (u:=s.query(UserPreferences).get(user_id)) is not None:
            return u.to_dict()

# borpa success database
# borpa_id : success

class BorpaScore(Base):
    __tablename__ = "borpascore"

    borpa_id: Mapped[str] = mapped_column(primary_key=True)
    score: Mapped[float]

def writeBorpaScore(borpa_id:str, score:float):
    with Session(engine) as s, s.begin():
        s.merge(BorpaScore(
            borpa_id = borpa_id,
            score = score
        ))

def getBorpaScore(borpa_id:str):
    with Session(engine) as s, s.begin():
        return s.query(BorpaScore).get(borpa_id).score

def getBorpaScoreSessionQuery() -> Tuple[Session, Query]:
    s = Session(engine)
    return s, s.query(BorpaScore)

# //////////////
# home screen
# /////////////

# upload 

class UploadCooldown(Base):
    __tablename__ = "uploadcooldown"

    user_id: Mapped[str] = mapped_column(primary_key=True)
    recent_uploads: Mapped[Optional[str]] = mapped_column(JSON)

def updateUploadCooldown(user_id:str) -> bool:  # True when allowed to upload, False when not
    with Session(engine) as s, s.begin():
        uploadCooldownObj = s.query(UploadCooldown).get(user_id)
        if uploadCooldownObj is None:
            s.add(UploadCooldown(
                user_id=user_id,
                recent_uploads=[time.time()]
            ))
            return True
        recent_uploads = uploadCooldownObj.recent_uploads.copy()
        recent_uploads.append(time.time())

        for upload_time in recent_uploads:
            time_diff = time.time() - upload_time
            if time_diff > 60:
                recent_uploads.remove(upload_time)
        uploadCooldownObj.recent_uploads = recent_uploads
        if (len(recent_uploads)<5):
            return True
        else:
            return False

#  counter

class CounterTypes:
    USER_ID = "counter_user"
    FILE_ID = "counter_user"
    BORPA_ID = "counter_borpa"

class Counter(Base):
    __tablename__ = "counter"

    id: Mapped[str] = mapped_column(primary_key=True)
    info: Mapped[int]

def getCounter(counter_type:str) -> int:
    with Session(engine) as s, s.begin():
        if (a:=s.query(Counter).get(counter_type)) is None:
            s.merge(Counter(
                id=counter_type,
                info=0
            ))
            return 0
        else:
            return a.info
    
def incrementCounter(counter_type:str, value:int=1) -> None:
    current_value = getCounter(counter_type)
    with Session(engine) as s, s.begin():
        s.merge(Counter(
            id=counter_type,
            info=current_value+value
        ))

class VerificationEmail(Base):
    __tablename__ = "verificationemail"

    user_id: Mapped[str] = mapped_column(primary_key=True)
    verification_code: Mapped[str]

def writeVerificationCode(user_id:str, verification_code:str):
    with Session(engine) as s, s.begin():
        s.merge(VerificationEmail(
            user_id = user_id,
            verification_code = verification_code
        ))

def resolveVerificationCode(verification_code:str) -> bool:
    with Session(engine) as s, s.begin():
        verification_code_obj = s.query(VerificationEmail).filter(VerificationEmail.verification_code==verification_code).first()
        if verification_code_obj is not None:
            user = getUser(verification_code_obj.user_id)
            user["profile_info"]["email_verified"] = True
            save_user(user, verification_code_obj.user_id)
            s.delete(verification_code_obj)
            return True
        return False
    
class RecoveryCode(Base):
    __tablename__ = "recoverycode"

    id: Mapped[int] = mapped_column(Integer, primary_key=True, autoincrement=True)
    user_id: Mapped[str]
    code: Mapped[str]

def writeRecoveryCode(user_id:str, recovery_code:str) -> None:
    with Session(engine) as s, s.begin():
        s.add(
            RecoveryCode(
                user_id = user_id,
                code = recovery_code
            )
        )

def resolveRecoveryCode(recovery_code:str) -> (bool, str):
    with Session(engine) as s, s.begin():
        resolve_code_obj = s.query(RecoveryCode).filter(RecoveryCode.code==recovery_code).first()
        if resolve_code_obj is not None:
            out_user_id = resolve_code_obj.user_id
            s.delete(resolve_code_obj)
            return True, out_user_id
        return False, None
    
class UserMarkedForDeletion(Base):
    __tablename__ = "deletedusers"

    user_id: Mapped[str] = mapped_column(primary_key=True)
    date: Mapped[float]

class IsMod(Base):
    __tablename__ = "ismod"

    user_id: Mapped[str] = mapped_column(primary_key=True)
    ismod: Mapped[bool]

def addMod(user_id:str):
    with Session(engine) as s, s.begin():
        s.merge(IsMod(
            user_id = user_id,
            ismod = True
        ))

def removeMod(user_id:str):
    with Session(engine) as s, s.begin():
        IsModObj = s.query(IsMod).get(user_id)
        IsModObj.ismod = False

def isMod(user_id:str) -> bool:
    if (user_id == "1"):
        return True
    with Session(engine) as s, s.begin():
        IsModObj = s.query(IsMod).get(user_id)
        if IsModObj is None:
            return False
        return IsModObj.ismod
    

class Party(Base):
    __tablename__ = "party4"

    __table_args__ = {}
    
    party_id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    game: Mapped[str]
    description: Mapped[str]
    max_members: Mapped[int]

    party_members: Mapped[str] = mapped_column(JSON) # list
    party_leader: Mapped[str]

    open: Mapped[bool]

    additional_info: Mapped[Optional[dict[str,str]]] = mapped_column(JSON(none_as_null=True))
    creation_date: Mapped[int] = mapped_column(DateTime(timezone=False), server_default=func.now())  # todo: delete after too old

    def to_dict(self):
        return {
            "party_id": self.party_id,
            "game": self.game,
            "description": self.description,
            "max_members": self.max_members,
            "party_members": self.party_members,
            "party_leader": self.party_leader,
            "open":self.open,
            "additional_info": self.additional_info
        }
    

class UserToParty(Base):
    __tablename__ = "usertoparty3"

    user_id: Mapped[str] = mapped_column(primary_key=True)
    party_id: Mapped[int]
    creation_date: Mapped[int] = mapped_column(DateTime(timezone=False), server_default=func.now())  # todo: delete after too old

def UserInParty(user_id:str):
    with Session(engine) as s, s.begin():
        if user_id is None:
            return None
        userToPartyObj = s.get(UserToParty ,user_id)
        if userToPartyObj is None:
            return None
        return userToPartyObj.party_id

async def create_party(game:str, description:str, max_members:int, party_leader:str, open:bool) -> int:
    additional_info = {}
    with Session(engine) as s, s.begin():
        if s.get(UserToParty, party_leader) is not None:  # user is already in party
            return

        new_party = Party(
            game = game,
            description = description,
            max_members = max_members,
            party_members = [party_leader],
            party_leader = party_leader,
            open = open,
            additional_info = additional_info
        )
        s.add(new_party)
        s.flush()
        party_id = new_party.party_id

        s.add(UserToParty(
            user_id = party_leader,
            party_id = party_id
        ))
    return party_id

def change_party_settings(party_id:int, changes:dict, changing_user_id:str) -> bool:
    allowed_changes = ["game", "description", "max_members", "party_leader"] 
    with Session(engine) as s, s.begin():
        party = s.get(Party, party_id)

        if changing_user_id != party.party_leader: return False

        if "game" in changes.keys():
            party.game = changes["game"]
        if "description" in changes.keys():
            party.description = changes["description"]
        if "max_members" in changes.keys():
            party.max_members= changes["max_members"] 
        if "party_leader" in changes.keys():
            party.party_leader = changes["party_leader"] 
        if "joining_method" in changes.keys():
            party.open = changes["joining_method"] == "open" 
    return True

def getParty(party_id:int) -> {}:
    with Session(engine) as s, s.begin():
        return s.get(Party, party_id).to_dict()

def getParties(game:Optional[str]):  # todo: add preferences in calculation
    count = 20
    with Session(engine) as s, s.begin():
        query = s.query(Party)
        if game is not None:
            query = query.filter(Party.game==game)
        return [i.to_dict() for i in query.order_by(func.random()).limit(count).all()]
            
        
class JoinPartyRequest(Base):
    __tablename__ = "joinpartyrequest"

    request_id: Mapped[int] = mapped_column(primary_key=True)
    party_id: Mapped[int]
    user_id: Mapped[str]
    creation_date: Mapped[int] = mapped_column(DateTime(timezone=False), server_default=func.now())  # todo: delete after too old
    

async def join_party(party_id:int, user_id:str) -> int: # 0: error, 1: send request, 2: joined
    with Session(engine) as s, s.begin():
        party = s.get(Party, party_id)
        user_to_party = s.get(UserToParty, user_id)
        if not user_to_party is None:
            return 0  # already in party
        if party.max_members <= len(party.party_members):
            return 0
        if party.open:
            party.party_members = party.party_members + [user_id]
            s.add(UserToParty(
                user_id=user_id,
                party_id=party_id
            ))
            for i in party.party_members[:-1]:
                await manager.sendToUser(i, web_socket_messages.NewPartyMember(user_id).to_dict())
            return 2
        s.add(JoinPartyRequest(
            party_id = party_id,
            user_id = user_id
        ))
        s.add(UserToParty(
            user_id = user_id,
            party_id = party_id
        ))
        return 1


def accept_join_request(party_id:int, accepted_user_id:str, accepting_user_id:str) -> bool:
    with Session(engine) as s, s.begin():
        party = s.get(Party, party_id)
        if not accepting_user_id in party.party_members:  # maybe should be party leader and not member, but for now members can accept requests too
            return False
        
        user_to_party = s.get(UserToParty, accepted_user_id)
        if not user_to_party is None:
            return 0  # already in party
        
        if party.max_members >= len(party.party_members):
            return False  # party too large
        
        request = s.query(JoinPartyRequest).filter(JoinPartyRequest.party_id==party_id and JoinPartyRequest.user_id == accepted_user_id).first()
        if request is None:
            return False  # user did not request to join

        s.delete(request)
        party.party_members = party.party_members + [accepted_user_id]
        s.add(UserToParty(
            user_id = accepted_user_id,
            party_id = party_id
        ))
        return True

async def leave_party(party_id:int, user_id:str) -> bool:
    with Session(engine) as s, s.begin():
        party = s.get(Party, party_id)
        if not user_id in party.party_members:
            return False
        temp = party.party_members.copy()
        temp.remove(user_id)
        if len(temp) == 0:
            s.delete(party)
        else:
            if party.party_leader == user_id:  # elect new party leader if old user leaves
                party.party_leader = temp[0]
        party.party_members = temp
        user_to_party = s.get(UserToParty, user_id)
        if user_to_party is not None:
            s.delete(user_to_party)
        return True

async def kick_party_member(party_id:int, kicked_player:str, kicking_player:str) -> bool:
    with Session(engine) as s, s.begin():
        party = s.get(Party, party_id)
        if not kicked_player in party.party_members:
            return False
        if kicking_player != party.party_leader:
            return False
        temp = party.party_members.copy()
        temp.remove(kicked_player)
        party.party_members = temp
        user_to_party = s.get(UserToParty, kicked_player)
        if user_to_party is not None:  # fixes errors, not needed in normal state
            s.delete(user_to_party)
        manager.sendToUser(kicked_player, web_socket_messages.PlayerPartyRemovedMessage().to_dict())
        return True  

async def delete_party(party_id:int, deleting_user:str):
    with Session(engine) as s, s.begin():
        party = s.get(Party, party_id)
        if deleting_user != party.party_leader and not isMod(deleting_user):
            return False
        s.delete(party)
        s.query(UserToParty).filter(UserToParty.party_id==party_id).delete()
        s.query(PartyMessage).filter(PartyMessage.party_id==party_id).delete()
        return True

class PartyMessage(Base):
    __tablename__ = "partymessage"

    message_id: Mapped[int] = mapped_column(primary_key=True, autoincrement=True)
    party_id: Mapped[str]
    user_id: Mapped[str]
    text: Mapped[str] = mapped_column(String(200))

    def to_dict(self):
        return {
            "message_id" : self.message_id,
            "party_id" : self.party_id,
            "user_id" : self.user_id,
            "text" : self.text
        } 

def send_party_message(party_id:int, user_id:str, text:str):  # maybe anti spam?
    with Session(engine) as s, s.begin():
        party = s.get(Party, party_id)
        if not user_id in party.party_members:
            return False
        s.add(PartyMessage(
            party_id = party_id,
            user_id = user_id,
            text = text
        ))
        return True

def delete_party_message(message_id:int, user_id:str):
    with Session(engine) as s, s.begin():
        message = s.get(PartyMessage, message_id)
        if message.user_id != user_id:  # only author can delete
            return False
        s.delete(message)

def get_party_messages(party_id:int, user_id:str) -> Optional[list]:
    message_limit = 100

    with Session(engine) as s, s.begin():
        party = s.get(Party, party_id)
        if not user_id in party.party_members:
            return None
        messages = s.query(PartyMessage).filter(PartyMessage.party_id==party_id).limit(message_limit).all()
        message_dicts = [i.to_dict() for i in messages]
        for i in message_dicts:
            i["author_profile_picture"] = getUserProfileInfo(i["user_id"])["profile_info"]["profile_picture"]
            i["author_name"] = getUserProfileInfo(i["user_id"])["profile_info"]["name"]
        return message_dicts
    

class ConnectionManager:
    def __init__(self):
        self.user_id_to_active_connections: dict[str,WebSocket] = {}

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        response = await websocket.receive()
        if response['type'] != 'websocket.receive': 
            try: return await websocket.close() 
            except: return
        user_id = get_current_user_by_session_id(response['text'])
        self.user_id_to_active_connections[user_id] = websocket
        return user_id

    def disconnect(self, user_id:str):
        self.user_id_to_active_connections.pop(user_id, None)

    async def sendData(self, websocket: WebSocket, data:{}):
        await websocket.send_json(data)  # maybe send as json
    
    async def sendToUser(self, user_id:str, data:{}):
        if user_id not in self.user_id_to_active_connections.keys():
            return
        websocket = self.user_id_to_active_connections[user_id]
        await self.sendData(websocket, data)

manager = ConnectionManager()


Base.metadata.create_all(engine)