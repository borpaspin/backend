from pysteamsignin.steamsignin import SteamSignIn
import requests
from . import helper_base
from .gamedata import IGameData, DOTA2, KDA
import json

url:str = "http://127.0.0.1:8000"
key = "8F806BD14DA5C8CA0150C27ABBD55102"

openDotaAPIUrlPlayers = "https://api.opendota.com/api/players/"

class helper(helper_base.helper):
    gameName = DOTA2
    def get_data(self):
        openDotaResponse = requests.get(openDotaAPIUrlPlayers+self.id)
        profile_data = json.loads(openDotaResponse.text)

        recentGamesResponse = requests.get(openDotaAPIUrlPlayers+self.id+"/recentMatches")
        recentgames_data = json.loads(recentGamesResponse.text)
        recent_games = []
        for i in recentgames_data[:5]:
            recent_games.append({
                "kda":KDA(i["kills"], i["deaths"], i["assists"]).toString(),
                "duration":i["duration"],
                "game_mode":handleGameMode(i["game_mode"]),
                "hero":heroIDDict[i["hero_id"]],
                "date":i["start_time"],
                "win":(i["radiant_win"] and i["player_slot"] < 128) or ((not i["radiant_win"]) and i["player_slot"] > 128),
                "match_id":i["match_id"]
            })
        if "mmr_estimate" in profile_data.keys():
            mmr = profile_data["mmr_estimate"]["estimate"]
        else:
            mmr = "unknown "
        try:
            return IGameData(
                gameUserID=self.id,
                gameName=self.gameName,
                inGameName=profile_data["profile"]["personaname"],
                profilePicture=profile_data["profile"]["avatarfull"],
                lastSeenOnline=profile_data["profile"]["last_login"],  # needs decoration
                optionalData={
                    'mmr':mmr,
                    'rank':getRank(profile_data["rank_tier"]),
                    'recent_games':recent_games
                }
            ).toDict()
        except Exception as e:
            return IGameData(
                gameName=self.gameName,
                inGameName="Account Private"
            ).toDict()
    def get_linking_site(self):
        steamLogin = SteamSignIn()
        return steamLogin.RedirectUser(steamLogin.ConstructURL(url+"/listener/steam/"+self.id)).headers["location"]
    
    # special functions
    def hasDotaAccount(self) -> bool:
        openDotaResponse = requests.get(openDotaAPIUrlPlayers+self.id)
        profile_data = json.loads(openDotaResponse.text)
        return "profile" in profile_data.keys()
    

def getRank(rank_num:int) -> str:
    rank_num = str(rank_num)
    rank = rank_num[0]
    stars = rank_num[1]
    rankIntToString = {
        1:"Herald",
        2:"Guardian",
        3:"Crusader",
        4:"Archon",
        5:"Legend",
        6:"Ancient",
        7:"Divine",
        8:"Immortal"
    }
    rank_str = rankIntToString[int(rank)]
    if rank_str=="Immortal": return rank_str
    return rank_str+stars

def handleGameMode(game_mode_id:int):
    game_modes = {
        1 : "All Pick",
        2 : "Captain's Mode",
        3 : "Random Draft",
        4 : "Single Draft",
        5 : "All Random",
        7 : "Diretide",
        8 : "Reverse Captain's Mode",
        9 : "Greeviling",
        10 : "Tutorial",
        11 : "Mid Only",
        12 : "Least Played",
        13 : "New Player Pool",
        14 : "Compendium Matchmaking",
        15 : "Custom",
        16 : "Captain's Draft",
        17 : "Balanced Draft",
        18 : "Ability Draft",
        20 : "All Random Deathmatch",
        21 : "Solo Mid 1v1",
        22 : "Ranked All Pick",
        23 : "Turbo"
    }
    return game_modes[game_mode_id]

heroIDDict = {
    1:"Anti-Mage",
    2:"Axe",
    3:"Bane",
    4:"Bloodseeker",
    5:"Crystal Maiden",
    6:"Drow Ranger",
    7:"Earthshaker",
    8:"Juggernaut",
    9:"Mirana",
    11:"Shadow Fiend",
    10:"Morphling",
    12:"Phantom Lancer",
    13:"Puck",
    14:"Pudge",
    15:"Razor",
    16:"Sand King",
    17:"Storm Spirit",
    18:"Sven",
    19:"Tiny",
    20:"Vengeful Spirit",
    21:"Windranger",
    22:"Zeus",
    23:"Kunkka",
    25:"Lina",
    31:"Lich",
    26:"Lion",
    27:"Shadow Shaman",
    28:"Slardar",
    29:"Tidehunter",
    30:"Witch Doctor",
    32:"Riki",
    33:"Enigma",
    34:"Tinker",
    35:"Sniper",
    36:"Necrophos",
    37:"Warlock",
    38:"Beastmaster",
    39:"Queen of Pain",
    40:"Venomancer",
    41:"Faceless Void",
    42:"Wraith King",
    43:"Death Prophet",
    44:"Phantom Assassin",
    45:"Pugna",
    46:"Templar Assassin",
    47:"Viper",
    48:"Luna",
    49:"Dragon Knight",
    50:"Dazzle",
    51:"Clockwerk",
    52:"Leshrac",
    53:"Nature's Prophet",
    54:"Lifestealer",
    55:"Dark Seer",
    56:"Clinkz",
    57:"Omniknight",
    58:"Enchantress",
    59:"Huskar",
    60:"Night Stalker",
    61:"Broodmother",
    62:"Bounty Hunter",
    63:"Weaver",
    64:"Jakiro",
    65:"Batrider",
    66:"Chen",
    67:"Spectre",
    69:"Doom",
    68:"Ancient Apparition",
    70:"Ursa",
    71:"Spirit Breaker",
    72:"Gyrocopter",
    73:"Alchemist",
    74:"Invoker",
    75:"Silencer",
    76:"Outworld Destroyer",
    77:"Lycan",
    78:"Brewmaster",
    79:"Shadow Demon",
    80:"Lone Druid",
    81:"Chaos Knight",
    82:"Meepo",
    83:"Treant Protector",
    84:"Ogre Magi",
    85:"Undying",
    86:"Rubick",
    87:"Disruptor",
    88:"Nyx Assassin",
    89:"Naga Siren",
    90:"Keeper of the Light",
    91:"Io",
    92:"Visage",
    93:"Slark",
    94:"Medusa",
    95:"Troll Warlord",
    96:"Centaur Warrunner",
    97:"Magnus",
    98:"Timbersaw",
    99:"Bristleback",
    100:"Tusk",
    101:"Skywrath Mage",
    102:"Abaddon",
    103:"Elder Titan",
    104:"Legion Commander",
    106:"Ember Spirit",
    107:"Earth Spirit",
    108:"Underlord",
    109:"Terrorblade",
    110:"Phoenix",
    105:"Techies",
    111:"Oracle",
    112:"Winter Wyvern",
    113:"Arc Warden",
    114:"Monkey King",
    119:"Dark Willow",
    120:"Pangolier",
    121:"Grimstroke",
    123:"Hoodwink",
    126:"Void Spirit",
    128:"Snapfire",
    129:"Mars",
    135:"Dawnbreaker",
    136:"Marci",
    137:"Primal Beast",
    138:"Muerta",
}
