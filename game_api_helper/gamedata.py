from typing import Optional

class IGameData:
    gameUserID:str
    gameName:str
    inGameName:str
    playtime:float
    profilePicture:str
    state:Optional[str]
    lastSeenOnline:Optional[str]
    friends:Optional[list[dict]]
    archievements:Optional[list]
    optionalData:Optional[dict]

    def __init__(
        self,
        gameUserID:str = "",
        gameName:str = "",
        inGameName:str = "",
        playtime:float = -1,
        profilePicture:str = "",
        state:str = None,
        lastSeenOnline:str = None,
        friends:list[dict] = [],
        archievements:list = [],
        optionalData:dict = {}
    ):
        self.gameUserID = gameUserID
        self.gameName = gameName
        self.inGameName = inGameName
        self.playtime = playtime
        self.profilePicture = profilePicture
        self.state = state
        self.lastSeenOnline = lastSeenOnline
        self.friends = friends
        self.archievements = archievements
        self.optionalData = optionalData


    def toDict(self) -> {}:
        return {
            "gameUserID":self.gameUserID,
            "gameName":self.gameName,
            "inGameName":self.inGameName,
            "playtime":self.playtime,
            "profilePicture":self.profilePicture,
            "state":self.state,
            "lastSeenOnline":self.lastSeenOnline,
            "friends":self.friends,
            "archievements":self.archievements,
            "optionalData":self.optionalData
        }
    
class KDA:
    kills:int
    deaths:int
    assists:Optional[int]

    def __init__(self, kills, deaths, assists):
        self.kills = kills
        self.deaths = deaths
        self.assists = assists

    def toString(self):
        return str(self.kills)+"/"+str(self.deaths)+"/"+str(self.assists)
    def getKDRatio(self):
        if self.deaths < 0: return -1
        return self.kills+self.assists/(self+self.deaths)


STEAM = "steam"
EPIC_GAMES = "epic_games"
LEAGUE_OF_LEGENDS = "league_of_legends"
DOTA2 = "dota2"
FORTNITE = "fortnite"
CLASH_ROYALE = "clash_royale"

class GameChilds:
    childs = {}
    childs[STEAM] = [DOTA2]
    childs[EPIC_GAMES] = [FORTNITE]

    def getChilds(parent:str) -> []:
        if parent not in GameChilds.childs.keys():
            return []
        return GameChilds.childs[parent]
    


