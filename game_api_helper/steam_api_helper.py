from datetime import datetime, timedelta
from pysteamsignin.steamsignin import SteamSignIn
import requests
from . import helper_base
from .gamedata import IGameData, STEAM
from steamid_converter import Converter
from util.url_config import backend_url, frontend_url
from .steam_api_key import key


class helper(helper_base.helper):
    gameName = STEAM
    def get_data(self):
        account_info = GetPlayerSummaries(self.id)
        friends = GetFriendList(self.id)
        owned_games = GetOwnedGames(self.id)
        recently_played_games = GetRecentlyPlayedGames(self.id)
        if "games" in recently_played_games:
            archievements = [GetPlayerAchievements(self.id, i["appid"]) for i in recently_played_games["games"]]
        else:
            archievements = []
        return IGameData(
            gameUserID=self.id,
            gameName=self.gameName,
            inGameName=account_info["username"],
            profilePicture=account_info["avatar"],
            state=account_info["userstate"],
            lastSeenOnline=account_info["timediff"],
            friends=friends,
            archievements=archievements,
            optionalData={
                "ownedGames":owned_games,
                "recentlyPlayedGames":recently_played_games["games"]
            }
        ).toDict()
    def get_linking_site(self):
        steamLogin = SteamSignIn()
        return steamLogin.RedirectUser(steamLogin.ConstructURL(backend_url+"/listener/steam/"+self.id)).headers["location"]
    def processLogin(self, request):
        steamLogin = SteamSignIn()
        self.id = steamLogin.ValidateResults(request.query_params)
        return self.id



def get_TimeDiff(lastlogoff) -> str:
    lastlogoff = datetime.fromtimestamp(lastlogoff)
    current_datetime = datetime.now()
    time_difference = current_datetime - lastlogoff
    intervals = [
        ("1 year", timedelta(days=365)),
        ("1 month", timedelta(days=30)),
        ("1 week", timedelta(weeks=1)),
        ("1 day", timedelta(days=1)),
        ("12 hours", timedelta(hours=12)),
        ("6 hours", timedelta(hours=6)),
        ("3 hours", timedelta(hours=3)),
        ("1 hour", timedelta(hours=1))
    ]
    largest_interval = None
    for interval_name, interval_value in intervals:
        if time_difference >= interval_value:
            largest_interval = (interval_name, interval_value)
            break
    if largest_interval:
        time_difference_str = f"{largest_interval[0]} ago"
    else:
        minutes_difference = int(time_difference.total_seconds() // 60)
        time_difference_str = f"{minutes_difference} minutes ago"

    #print("Time Difference:", time_difference_str)
    return time_difference_str

def GetPlayerSummaries(steamID):
    user_info = requests.get(f'https://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key={key}&steamids={f"{steamID}"}').json()
    if user_info is not None:
        username = user_info['response']['players'][0]['personaname']
        avatar = user_info['response']['players'][0]['avatarfull'] # avatar, avatarmedium, avatarfull
        userstate = user_info['response']['players'][0]['personastate']
        try:
            lastlogoff = user_info['response']['players'][0]['lastlogoff']
            timediff = get_TimeDiff(lastlogoff)
        except:
            lastlogoff = "unknown"
            timediff = "unknown"

        if userstate == 0:
            userstate = "Offline"
        elif userstate == 1:
            userstate = "Online"
        elif userstate == 2:
            userstate = "Busy"
        elif userstate == 3:
            userstate = "Away"
        elif userstate == 4:
            userstate = "Snooze"
        elif userstate == 5:
            userstate = "looking to trade"
        elif userstate == 6:
            userstate = "looking to play"

        Summaries = {
            "username" : username,
            "avatar" : avatar,
            "userstate" : userstate,
            "timediff" : timediff
        }

        return Summaries
    else:
        return print('Failed to retrieve user information')
    
def GetFriendList(steamID):
    user_friendlist = requests.get(f'http://api.steampowered.com/ISteamUser/GetFriendList/v0001/?key={key}&steamid={f"{steamID}"}&relationship=friend').json()
    if user_friendlist != {} and not user_friendlist is None:
        friends = user_friendlist["friendslist"]["friends"]
        return friends
    else:
        return print("Failed to retrieve user friendlist information")
    
def GetOwnedGames(steamID):
    user_ownedGames = requests.get(f'http://api.steampowered.com/IPlayerService/GetOwnedGames/v0001/?key={key}&steamid={f"{steamID}"}&include_appinfo=1&include_played_free_games=1&format=json').json()
    if user_ownedGames is not None and user_ownedGames["response"]!={}:
        ownedGames = user_ownedGames["response"]
        c = 0
        for game in ownedGames["games"]:
            game["img_icon_url"] = f'http://media.steampowered.com/steamcommunity/public/images/apps/{game["appid"]}/{game["img_icon_url"]}.jpg'
            ownedGames["games"][c]["img_icon_url"] = game["img_icon_url"]
            c+=1
        return ownedGames
    else:
        return print("Failed to retrieve user ownedgame information")

def GetRecentlyPlayedGames(steamID):
    user_recentlyPlayedGames = requests.get(f'http://api.steampowered.com/IPlayerService/GetRecentlyPlayedGames/v0001/?key={key}&steamid={f"{steamID}"}&format=json').json()
    if user_recentlyPlayedGames is not None:
        recentlyPlayedGames = user_recentlyPlayedGames["response"]
        c = 0
        if "games" not in recentlyPlayedGames:  return recentlyPlayedGames
        for game in recentlyPlayedGames["games"]:
            game["img_icon_url"] = f'http://media.steampowered.com/steamcommunity/public/images/apps/{game["appid"]}/{game["img_icon_url"]}.jpg'
            recentlyPlayedGames["games"][c]["img_icon_url"] = game["img_icon_url"]
            c+=1
        return recentlyPlayedGames
    else:
        return print("Failed to retrieve user recentownedgame information")
    
def GetPlayerAchievements(steamID, appID):
    user_achievements = requests.get(f'http://api.steampowered.com/ISteamUserStats/GetPlayerAchievements/v0001/?appid={f"{appID}"}&key={key}&steamid={f"{steamID}"}&l=1').json()
    if user_achievements is not None:
        achievements = user_achievements["playerstats"]
        if not achievements["success"]: return
        accomplished = []
        todo = []
        if "archievements" not in achievements.keys(): return
        for subachiev in achievements["achievements"]:
            if subachiev["achieved"] == 1:
                accomplished.append(subachiev)
            else:
                todo.append(subachiev)
        achievements["achievements"] = [accomplished, todo]
        return achievements
    else:
        return print("Failed to retrieve user achievement information")

def steam64tosteam3(steam_id64:str) -> str:
    return Converter.to_steamID3(steam_id64).split(":")[-1].replace("]", "")
