from .gamedata import IGameData, FORTNITE
import fortnite_api
from .fortnite_api_key import key
from . import helper_base
from util.url_config import backend_url, frontend_url

api = fortnite_api.FortniteAPI(api_key=key)

class helper(helper_base.helper):
    def get_data(self):
        stats = api.stats.fetch_by_id(self.id)
        stats
        return IGameData(
            gameUserID=id,
            gameName=FORTNITE,
            inGameName=stats.user.name
        )
    
    def get_linking_site(self) -> str:
        return frontend_url+"/linking/epic-games"


