from .gamedata import IGameData, CLASH_ROYALE
from . import helper_base
from .clash_royale_key import key
import clashroyale
from util.url_config import backend_url, frontend_url

client = clashroyale.official_api.Client(key)

class helper(helper_base.helper):
    def get_data(self):
        player_data = client.get_player(self.id).raw_data
        match_history = client.get_player_battles(self.id)
        return IGameData(
            gameUserID=self.id,
            gameName=CLASH_ROYALE,
            inGameName=player_data["name"],
            archievements=player_data["achievements"],
            optionalData={
                "rank":[player_data["currentPathOfLegendSeasonResult"]["trophies"], player_data["currentPathOfLegendSeasonResult"]["leagueNumber"]],  # trophies in path of legend
                "mmr":player_data["trophies"],  # trophies in normal mode
                "highest_rank":player_data["bestPathOfLegendSeasonResult"]["rank"],  # None if not a high rank
                "recent_games":[match_history[i].raw_data for i in range(5)]
            }
        ).toDict()
    
    def get_linking_site(self) -> str:
        return frontend_url+"/linking/clash-royale"
    
    def player_exists(id) -> bool:
        try:
            player_data = client.get_player(id).raw_data
            return True
        except ValueError:
            return False