from . import steam_api_helper, dotabuff_helper, u_gg_helper, epic_games_api_helper, gamedata, clash_royale_helper


STEAM = gamedata.STEAM
EPIC_GAMES = gamedata.EPIC_GAMES
LEAGUE_OF_LEGENDS = gamedata.LEAGUE_OF_LEGENDS
DOTA2 = gamedata.DOTA2
CLASH_ROYALE = gamedata.CLASH_ROYALE

HELPER_DICT = {}
HELPER_DICT[STEAM] = STEAM_HELPER = steam_api_helper.helper
HELPER_DICT[EPIC_GAMES] = EPIC_GAMES_HELPER = epic_games_api_helper.helper
HELPER_DICT[LEAGUE_OF_LEGENDS] = LEAGUE_OF_LEGENDS_HELPER = u_gg_helper.helper
HELPER_DICT[DOTA2] = DOTA2_HELPER = dotabuff_helper.helper
HELPER_DICT[CLASH_ROYALE] = CLASH_ROYALE_HELPER = clash_royale_helper.helper

