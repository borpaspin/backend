from .gamedata import IGameData, EPIC_GAMES
import fortnite_api
from .fortnite_api_key import key
from . import helper_base
from util.url_config import backend_url, frontend_url

class helper(helper_base.helper):
    def get_data(self):
        return {}
    
    def get_linking_site(self) -> str:
        return frontend_url+"/linking/epic-games"