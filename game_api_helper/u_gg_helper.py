from game_api_helper.gamedata import IGameData
from . import helper_base
from .gamedata import IGameData, LEAGUE_OF_LEGENDS, KDA
#  import cassiopeia as cass
#  from cassiopeia import Summoner
import riotwatcher
from riotwatcher import LolWatcher
from util.url_config import backend_url, frontend_url
from .riot_api_key import riot_api_key
from typing import List, Any
import requests
import json

league_api_key = riot_api_key  # muss noch versteckt werden
#cass.set_riot_api_key(league_api_key)
watcher = LolWatcher(league_api_key)

class helper(helper_base.helper):
    summoner = None
    def get_data(self):
        region = self.id.split("|")[1].lower()+"1"
        account_id = self.id.split("|")[0]

        summoner = watcher.summoner.by_id(region, account_id)
        self.summoner = summoner

        rank, division, lp, winloss = handleRankedStats(getRequest(region+".api.riotgames.com/lol/league/v4/entries/by-summoner/"+account_id))
        #  current_match = watcher.spectator.by_summoner(region, account_id)
        match_history_ids = watcher.match.matchlist_by_puuid(region=region, puuid=summoner['puuid'], count=5)
        match_history = self.handleMatchHistory([watcher.match.by_id(region, match_id) for match_id in match_history_ids])
        masteries = watcher.champion_mastery.by_summoner(region, account_id)
        #  watcher.challenges  sind archievements
        out = IGameData(
            gameUserID=self.id,
            gameName=LEAGUE_OF_LEGENDS,
            inGameName=summoner['name'],
            profilePicture=summoner['profileIconId'], # id
            state="",  # work in progress
            lastSeenOnline=0,  # work in progress
            friends=[],  # impossible
            archievements=masteries[:5],  # nicht wirklich archievements
            optionalData={
                'mmr':lp,  # ?
                'rank':rank+" "+division,  # ?
                'recent_games':match_history,
                'level':summoner['summonerLevel'],
                'region':region
            }
        ).toDict()
        return out
    
    def get_linking_site(self):
        return frontend_url+"/linking/league"
    
    def getUserID(name:str, region:str):
        try:
            summoner = watcher.summoner.by_name(region.lower()+"1", name)
            return summoner['id']
        except:
            return
        
    def handleMatchHistory(self, matchHistory:List[Any]):
        out = []
        for i in matchHistory:
            matchID = i['metadata']['matchId']
            i = i['info']
            player = None
            for user in i['participants']:
                if user['puuid'] == self.summoner['puuid']:
                    player = user
                    break
            out.append({
                "kda":KDA(player["kills"], player["deaths"], player["assists"]).toString(),
                "duration":i["gameDuration"],
                "game_mode":handleGameMode(i["gameMode"], i["gameType"]),
                "hero":player["championName"],
                "hero_id":player["championId"],
                "date":i["gameCreation"]/1000,
                "win":player["win"],
                "match_id":matchID
            })

        return out
        

# check league's latest version
latest = watcher.data_dragon.versions_for_region("euw1")['n']['champion']


static_champ_list = watcher.data_dragon.champions(latest, False, 'en_US')
champ_dict = {}
for key in static_champ_list['data']:
    row = static_champ_list['data'][key]
    champ_dict[row['key']] = row['id']


static_item_list = watcher.data_dragon.items(latest, 'en_US')
item_dict = {}
for key in static_item_list['data']:
    row = static_item_list['data'][key]
    item_dict[key] = row['name']


def getRequest(url):
    if url[0]!="h":
        url = "https://"+url
    headers = { "X-Riot-Token":league_api_key}
    return json.loads(requests.get(url=url, headers=headers).text)

def handleRankedStats(rankedStats):
    for i in rankedStats:
        if i["queueType"]=='RANKED_SOLO_5x5':
            return i['tier'], i['rank'], i['leaguePoints'], str(i['wins'])+"/"+str(i['losses'])  # rank, division, lp, win/loss
    return "unranked", "", None, None
        
def handleGameMode(gameMode, gameType):  # keine ahnung
    gameModeDict = {
        'CLASSIC':'Normal Draft',  # kein ahnung ob das stimmt
        'CHERRY':'Arena Ranked',
    }
    try:
        return gameModeDict[gameMode]
    except:
        return gameMode
    
def handleMasteries(masteries):
    return masteries