from itertools import count
from re import U
from urllib import response
from fastapi import FastAPI, Response, Request, Form, File, UploadFile, Query, WebSocket, WebSocketDisconnect
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import JSONResponse
from fastapi.staticfiles import StaticFiles
from typing import Union
import database
from util import password_hash, timer_decorator, check_login, email_validator, goodchars
from threading import Thread
import random
import string
import uvicorn
from app_features import home, search
import game_data_collector
from typing import Any, List
from pydantic import BaseModel
from typing_extensions import Annotated
import os
import sys
from game_api_helper.steam_api_helper import steam64tosteam3
from validate_email import validate_email
from util.url_config import backend_url, frontend_url, shortend_backend_url
from fastapi.responses import RedirectResponse
from google.cloud.recaptchaenterprise_v1 import Assessment
from google.cloud import recaptchaenterprise_v1
from util.hidden_constants import recaptcha_site_key, project_id
from util.double_sided_dict import DoubleSidedDict

app = FastAPI()
ip = "127.0.0.1:8000"
url:str = shortend_backend_url

origins = [
    "*"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

client = recaptchaenterprise_v1.RecaptchaEnterpriseServiceClient()

@app.get("/")
def redirectWWW(q : Union[str, None]=None):
    return RedirectResponse(url=frontend_url)

app.mount("/image", StaticFiles(directory="database/images"), name="image")
patchNotes = {}
for i in os.listdir(os.getcwd()+"/database/patchnotes"):
    with open(os.getcwd()+"/database/patchnotes/"+i) as f:
        patchNotes[i] = f.read()
@app.get("/patchnotes/{name}")
def getPatchNotes(name:str, q : Union[str, None]=None):
    return patchNotes[name]

@app.post("/save_image/")
async def save_image(
    thumbnail: Annotated[bytes, File()], 
    filename: Annotated[str, File()], 
    request:Request,
    q : Union[str, None]=None
):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    user_id = database.get_current_user(request)
    if not database.updateUploadCooldown(user_id): return {"success":False, "message":"too many uploads in 1min"}
    # check file size
    if thumbnail.__len__()>10000000: return {"success":False, "message":"File too large"} 

    # check file format
    if not is_safe_path(os.getcwd(), filename): return {"success":True, "message":"Unsafe Filename"}
    accepted_types = ["png", "jpeg", "jpg", "mp4", "webm", "gif", "mp3", "wav"]  # wirklich videos und audio?
    in_file_extension:str = filename.split(".")[-1]
    if filename.split(".").__len__ == 1: return {"success":False, "message":"No extention"}
    if not in_file_extension in accepted_types: return {"success":False, "message":"Wrong file extention"}
    else: 
        if False: {"success":False, "message":"Data does not match file extention"}  # todo: testen ob das zutrifft

    database.incrementCounter(database.CounterTypes.FILE_ID)
    with open("database/images/"+str(database.getCounter(database.CounterTypes.FILE_ID))+filename, "wb") as f:
        f.write(thumbnail)
    
    return {"success":True, "message":"image saved successfully", "body":"".join((
        backend_url,
        "/image/",
        str(database.getCounter(database.CounterTypes.FILE_ID))
        ,filename
    ))}

@app.get("/borpa/{borpa_id}")
async def read_borpa(request: Request, borpa_id : str, q : Union[str, None]=None):
    if check_login.test(request, database):
        return database.read_borpa(borpa_id, getComments=True)
    else:
        user_id = database.get_current_user(request)
        user = database.getUser(user_id)
        borpa = database.read_borpa(borpa_id, getComments=True)
        borpa["liked"] = borpa_id in user["liked_borpas"]
        for comment in borpa["comments"]:
            comment["liked"] = user_id in comment["liking_users"]
            comment["liking_users"] = None
        return borpa

# Annotated[List[str], Form()]
@app.post("/borpas/")
async def read_borpas(request: Request, borpa_ids: Annotated[str, Form()], q : Union[str, None]=None):
    if check_login.test(request, database):  # not signed in
        borpa_ids = borpa_ids.split(",")
        return [database.read_borpa(i) for i in borpa_ids]
    else:
        user_id = database.get_current_user(request)
        user = database.getUser(user_id)
        borpas = [database.read_borpa(i) for i in borpa_ids]
        for borpa in borpas:
            borpa["liked"] = borpa["borpa_id"] in user["liked_borpas"]
        return borpas

@app.get("/user/{user_id}")  # censor password and email
async def read_user(user_id : int, q : Union[str, None]=None):
    user = database.read_user(user_id)
    user["profile_info"]["password"] = None
    user["profile_info"]["email"] = None
    user["borpas"] = user["borpas"][::-1]
    if not user["settings"]["publicFriendList"]:
        user["friends"] = None
    return user

@app.post("/users/")
async def read_users(user_ids: Annotated[str, Form()], q : Union[str, None]=None):
    user_ids = user_ids.split(",")
    return [database.read_user(i) for i in user_ids]

@app.post("/changesettings/")
async def change_settings(  
    request: Request,
    q : Union[str, None]=None
):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    changes_dict = await request.json()
    user_id = database.get_current_user(request)
    user_dict = database.read_user(user_id)
    bad_return = False
    bad_return_value = {}
    for i in changes_dict.keys():
        if hasattr(changes_dict[i], "keys"):  
            for i2 in changes_dict[i].keys():
                if i2=="name":
                    changes_dict[i][i2] = goodchars.removeBadChars(changes_dict[i][i2], goodchars.name)
                    database.updateUserIDFromName(database.get_current_user(request), database.getUser(database.get_current_user(request))["profile_info"]["name"], changes_dict[i][i2])
                if i2=="email":
                    if (validate_email(email=changes_dict[i][i2])):
                        if not database.changeEmail(changes_dict[i][i2], user_id):
                            bad_return = True
                            bad_return_value["email"] = "email taken"
                    else:
                        bad_return = True
                        bad_return_value["email"] = "email not existant"
                user_dict[i][i2] = changes_dict[i][i2]
        else:
            user_dict[i] = changes_dict[i]
    database.save_user(user_dict, database.get_current_user(request))
    if bad_return:
        return {"success":False, "message":"Something went wrong", "body":bad_return_value}
    return {"success":True, "message":"settings changed"}

@app.get("/getself/")
async def getLogedInUser(request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}  # test func
    user_id = database.get_current_user(request)
    user = database.read_user(user_id)  
    user["is_mod"] = database.isMod(user_id)
    return user

@app.get("/users/search/{name}")
async def search_user(name : str, q : Union[str, None]=None):
    return search.search_user(name, database)

@app.get("/test/")
def test(request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}  # test func
    return {"success":True, "message":"User is logged in"}
    
@app.post("/like/{borpa_id}")
def like(request:Request, borpa_id:str, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    user_id = database.get_current_user(request)
    user = database.read_user(user_id)
    if borpa_id in user["liked_borpas"]: return {"success":False, "message":"already liked"}
    database.writeBorpaScore(borpa_id, database.getBorpaScore(borpa_id)+1)
    borpa = database.read_borpa(borpa_id)
    borpa["likes"] += 1
    database.save_borpa(borpa)

    user["liked_borpas"].append(borpa_id)
    database.save_user(user, user_id)

    # game preferences
    game = database.read_borpa(borpa_id)["game"]

    if not game in (preferences:=database.getUserPreferences(user_id))["games"].keys():
        preferences["games"][game] = 0

    preferences["games"][game] += 0.2
    preferences["games"][game] *= 1.12
    for i in preferences["games"].keys(): # alle anderen spiele
        preferences["games"][i] *= 0.92

    # user preferences
    creator_id = borpa["user_id"]
    if not creator_id in preferences["users"].keys():
        preferences["users"][creator_id] = 0

    preferences["users"][creator_id] += 0.2
    preferences["users"][creator_id] *= 1.12
    for i in preferences["users"].keys():
        preferences["users"][i] *= 0.92

    database.writeUserPreferences(user_id, preferences)

    return {"success":True, "message":""}

@app.post("/remove_like/{borpa_id}")
def remove_like(request:Request, borpa_id:str, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    user_id = database.get_current_user(request)
    user = database.read_user(user_id)
    if not borpa_id in user["liked_borpas"]: return {"success":False, "message":"Borpa was not liked, like cant be removed"}
    user["liked_borpas"].remove(borpa_id)
    database.save_user(user, user_id)    

    database.writeBorpaScore(borpa_id, database.getBorpaScore(borpa_id)-1)
    borpa = database.read_borpa(borpa_id)
    borpa["likes"] -= 1
    database.save_borpa(borpa)
 
    # user preferences
    creator_id = database.read_borpa(borpa_id)["user_id"]
    if not creator_id in (preferences:=database.getUserPreferences(user_id))["users"].keys():
        preferences["users"][creator_id] = 0
    preferences["users"][creator_id] *= 1/1.12

    return {"success":True, "message":""}

@app.post("/write_comment/{borpa_id}")  # todo: cooldown
def addComment(request:Request, borpa_id:str, text:Annotated[str, Form()], q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    borpa = database.read_borpa(borpa_id)
    if borpa["text"]=="deleted" and borpa["body"]=="": return {"success":False, "message":"Post is deleted"}
    user_id = database.get_current_user(request)
    database.addComment(borpa_id, text, user_id)
    return {"success":True, "message":"comment saved"}

@app.post("/write_subcomment/{comment_id}")
def addSubcomment(request:Request, comment_id:str, text:Annotated[str, Form()], q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    user_id = database.get_current_user(request)
    database.addSubcomment(comment_id, text, user_id)
    return {"success":True, "message":"subcomment saved"}

@app.get("/get_subcomments/{comment_id}")
def getSubcomments(request:Request, comment_id:str, q : Union[str, None]=None):
    subcomments = database.getSubcomments(comment_id)
    user_id = None
    if not check_login.test(request, database):
        user_id = database.get_current_user(request)
    for subcomment in subcomments:
        author = database.getUser(subcomment["author_id"])
        subcomment["author_profile_picture"] = author["profile_info"]["profile_picture"]
        subcomment["author_name"] = author["profile_info"]["name"]
        if user_id is not None:
            subcomment["liked"] = user_id in subcomment["liking_users"]
        else:
            subcomment["liked"] = False
    return {"success":True, "message":"", "body":subcomments}

@app.get("/comment/like/{comment_id}")
async def likeComment(request:Request, comment_id:str, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    success = database.likeComment(comment_id, database.get_current_user(request))
    return {"success":success, "message":""}

@app.get("/comment/remove_like/{comment_id}")
async def removeLikeComment(request:Request, comment_id:str, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    success = database.removeLikeComment(comment_id, database.get_current_user(request))
    return {"success":success, "message":""}

@app.get("/subcomment/like/{comment_id}")
async def likeSubcomment(request:Request, comment_id:str, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    success = database.likeSubcomment(comment_id, database.get_current_user(request))
    return {"success":success, "message":""}

@app.get("/subcomment/remove_like/{comment_id}")
async def removeLikeSubcomment(request:Request, comment_id:str, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    success = database.removeLikeSubcomment(comment_id, database.get_current_user(request))
    return {"success":success, "message":""}


@app.get("/home/")
async def get_home(request:Request, q : Union[str, None]=None):
    if check_login.test(request, database):
        borpas = home.get_home_screen_borpas_standard(database)
    else:
        user_id = database.get_current_user(request)
        borpas = home.get_home_screen_borpas_logged_in(database, user_id)
    return {"success":True, "message":"returned borpas", "body":{"borpas":borpas, "authors":[database.read_user(i["user_id"], safe=True) for i in borpas]}}

@app.post("/friend_request/{user_id}")
async def friend_request(user_id:int,request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    current_user_id = database.get_current_user(request)
    current_user_dict = database.read_user(current_user_id)
    if user_id in current_user_dict["friends"]: return {"success":False, "message":"user is already friend"}
    current_user_dict["friends"].append(user_id)
    if not user_id in (preferences:=database.getUserPreferences(current_user_id))["users"].keys():
        preferences["users"][user_id] = 0
    preferences["users"][user_id] += 10
    database.writeUserPreferences(current_user_id, preferences)
    database.save_user(current_user_dict, current_user_id)
    return {"success":True, "message":""}


@app.post("/unfriend/{user_id}")
async def unfriend(user_id:int,request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    current_user_id = database.get_current_user(request)
    current_user_dict = database.read_user(current_user_id)
    if not user_id in current_user_dict["friends"]: return {"success":False, "message":"user is not a friend"}
    current_user_dict["friends"].remove(user_id)
    if not user_id in (preferences:=database.getUserPreferences(current_user_id))["users"].keys():
        preferences["users"][user_id] = 0
    preferences["users"][user_id] -= 10
    database.writeUserPreferences(current_user_id, preferences)
    database.save_user(current_user_dict, current_user_id)
    return {"success":True, "message":""}


@app.post("/create_borpa/")  # work in progress
async def new_borpa(
    request:Request, 
    game:Annotated[str, Form()], 
    body:Annotated[str, Form()], 
    text:Annotated[str, Form()], 
    linked_media:Annotated[str, Form()], 
    q : Union[str, None]=None
):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 

    database.incrementCounter(database.CounterTypes.BORPA_ID)
    counter_borpa = database.getCounter(database.CounterTypes.BORPA_ID)

    if len(text) > 280: return {"success":False, "message":"too long"}
    user_id = database.get_current_user(request)

    database.save_borpa(database.borpa(counter_borpa, user_id, game, body, text, linked_media))
    database.link_borpa_to_user(user_id, counter_borpa)
    database.writeBorpaScore(counter_borpa, 0)
    return {"success":True, "message":""}

@app.get("/create_user/{name}/{email}/{password}")  
async def new_user(request: Request, name:str, email:str, password:str, q : Union[str, None]=None):

    recaptcha_token = request.headers["recaptcha-token"]
    if not assess_captcha(recaptcha_token): return {"success":False, "message":"You seem to be a bot"}

    database.incrementCounter(database.CounterTypes.USER_ID)
    counter_user = database.getCounter(database.CounterTypes.USER_ID) 

    if database.getUserIDFromEmail(email)==None and validate_email(email):
        verification_id = password_hash.salted_hash(''.join(random.choices(string.ascii_letters, k=8)))
        email_validator.sendVerificationEmail(backend_url+"/listener/email/"+verification_id,email)
        database.writeVerificationCode(counter_user, verification_id)

        hashed_pw = password_hash.salted_hash(password)
        database.save_user(database.user_profile(counter_user, name, email, hashed_pw), counter_user)
        database.writeUserIDFromEmail(email, counter_user)
        database.writeUserIDFromName(counter_user, name)

        login_session_id = password_hash.salted_hash(''.join(random.choices(string.ascii_letters, k=10)))

        database.writeUserIDFromLoginSession(counter_user, login_session_id)
        response = JSONResponse(content={"success":True, "message":"", "cookies":[["login_session_id"], [login_session_id]]})
        return response
    response = JSONResponse(content={"success":False, "message":"Email taken or invalid"})
    return response

@app.get("/verifyemail")
def resendVerificationEmail(request:Request,q : Union[str, None]=None):
    user_id = database.get_current_user(request)
    verification_id = password_hash.salted_hash(''.join(random.choices(string.ascii_letters, k=8)))
    user = database.getUser(user_id)
    email = user["profile_info"]["email"]
    email_validator.sendVerificationEmail(backend_url+"/listener/email/"+verification_id,email)
    database.writeVerificationCode(user_id, verification_id)
    return {"success":True}

@app.get("/recover-password/{email}")
def recoverPassword(request:Request, email:str, q : Union[str, None]=None):
    user_id = database.getUserIDFromEmail(email)
    if user_id == None:
        return {"success":False, "message":"email nonexistant"}
    recovery_code = password_hash.salted_hash(password_hash.salted_hash(''.join(random.choices(string.ascii_letters, k=10))))
    database.writeRecoveryCode(user_id, recovery_code)
    email_validator.send_message(
        email_validator.service,
        email,
        "Recover your borpaspin account",
        """
        Dear Borpaspin User,
        
        Click this link to reset your password: {}""".format(frontend_url+"/reset-password?recovery_code="+recovery_code)  # todo
    )
    return {"success":True, "message":"email sent"}

@app.post("/set-password/{recovery_code}")
def setPassword(request:Request, recovery_code:str, new_password:Annotated[str, Form()], q : Union[str, None]=None):
    is_valid, user_id = database.resolveRecoveryCode(recovery_code)
    hashed_password = password_hash.salted_hash(new_password)
    if not is_valid:
        return {"success":False, "message":"recovery code false"}
    user = database.getUser(user_id)
    user["profile_info"]["password"] = hashed_password
    database.save_user(user, user_id)
    return {"success":True, "message":"changed pw"}

@app.get("/listener/email/{verification_id}")
def verifyUserEmail(request:Request,verification_id:str,q : Union[str, None]=None):
    if database.resolveVerificationCode(verification_id):
        return "Successfully linked account. You can close this window now."
    return "Error accured. Account not verified."

@app.get("/login/{email}/{password}")
async def login(request:Request, password:str,email:str,q : Union[str, None]=None):
    recaptcha_token = request.headers["recaptcha-token"]
    if not assess_captcha(recaptcha_token): return {"success":False, "message":"bot detected"}
    hashed_pw = password_hash.salted_hash(password)
    if (account_id:=database.getUserIDFromEmail(email)) is None: return {"success":False, "message":"wrong email", "body":"email"}
    account_dict = database.getUserProfileInfo(account_id, safe=False)
    if account_dict["profile_info"]["password"] == hashed_pw:
        print("user"+str(account_id)+" logged in")
        login_session_id = password_hash.salted_hash(''.join(random.choices(string.ascii_letters, k=10))+str(account_id))
        database.writeUserIDFromLoginSession(account_id, login_session_id)
        response = JSONResponse(content={"success":True, "message":"", "cookies":[["login_session_id"], [login_session_id]]})
        return response
    else:
        response = JSONResponse(content={"success":False, "message":"wrong password", "body":"pw"})
        return response
    
@app.get("/verifypw/{password}")
def verifyPassword(request:Request, password:str,q : Union[str, None]=None):
    hashed_pw = password_hash.salted_hash(password)
    account_id = database.get_current_user(request)
    account_dict = database.read_user(account_id)
    return account_dict["profile_info"]["password"] == hashed_pw

@app.post("/special-settings-change/changepw")  # maybe cooldown
def changePassword(request:Request, password:Annotated[str, Form()],q : Union[str, None]=None):
    hashed_pw = password_hash.salted_hash(password)
    account_id = database.get_current_user(request)
    account_dict = database.read_user(account_id)
    account_dict["profile_info"]["password"] = hashed_pw
    database.save_user(account_dict, account_id)
    return {"success":True, "message":"Password changed"}
    
@app.get("/delete/user")
def delete_user(request:Request,q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    user_id = database.get_current_user(request)
    body = database.del_user(user_id)
    if not body:
        email = database.getUser(user_id)["profile_info"]["email"]
        email_validator.send_message(email_validator.service, email, "borpaspin.net account deletion",
            """Dear borpaspin user,
            
            your account is about to get deleted. If this was not you
            or was a mistake, please contact us immediatly to
            recover your account.
            
            Your borpaspin team""")
    return {"success":True, "message":"Account marked for deletion", "body":body}

@app.get("/delete/borpa/{borpa_id}")
def delete_user(request:Request, borpa_id:str, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    user_id = database.get_current_user(request)
    # check if user is owner of borpa
    borpa = database.read_borpa(borpa_id)
    if (borpa["user_id"] != user_id and not database.isMod(user_id)): return {"success":False, "message":"User is not owner of borpa"} #  a mod can delete other users posts

    database.del_borpa(borpa_id)

    return {"success":True, "message":"Borpa deleted"}


# account linking
@app.get("/listener/steam/{user_id}")   # ist glaub ich durch validate results safe vielleicht aber auch net
def steamListener(request:Request, user_id:str, q : Union[str, None]=None):
    steam_id = game_data_collector.game_const.STEAM_HELPER(user_id).processLogin(request)
    if (steam_id is None): return "Internal Error: Steam ID is null"
    user = database.read_user(user_id)
    user["game_data"]["steam"] = steam_id
    steam_id3 = steam64tosteam3(steam_id)
    if game_data_collector.game_const.DOTA2_HELPER(steam_id3).hasDotaAccount():
        user["game_data"][game_data_collector.game_const.DOTA2] = steam_id3
    database.save_user(user, user_id)
    return "Your steam account is now linked."

@app.get("/listener/league/{name}/{region}")
def leagueListener(request:Request, name:str, region, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    league_id = game_data_collector.game_const.LEAGUE_OF_LEGENDS_HELPER.getUserID(name, region)
    if league_id is None: return {"success":False, "message":"Not known"}
    user_id = database.get_current_user(request)
    user = database.read_user(user_id)
    user["game_data"][game_data_collector.game_const.gamedata.LEAGUE_OF_LEGENDS] = league_id+"|"+region
    database.save_user(user, user_id)
    return {"success":True, "message":"You League of Legends account is now linked."} 

@app.get("/listener/epic-games/{name}")
def epicgamesListener(request:Request, name:str, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}

@app.get("/listener/clash-royale/{name}")
def clashroyaleListener(request:Request, name:str, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    if not game_data_collector.game_const.CLASH_ROYALE_HELPER.player_exists(name): return {"success":False, "message":"Not known player"}
    user_id = database.get_current_user(request)
    user = database.read_user(user_id)
    user["game_data"][game_data_collector.game_const.gamedata.CLASH_ROYALE] = name
    database.save_user(user, user_id)
    return {"success":True, "message":"You Clash Royale account is now linked."} 


@app.get("/getLinkingSite/{game}")
def getLinkingSite(game:str,request:Request, q : Union[str, None]=None) -> str:  # url
    out = game_data_collector.getLinkingSite(game, database.get_current_user(request))
    return out

@app.get("/unlink-game/{game}")
def unlinkGame(game:str, request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"}
    user_id = database.get_current_user(request)
    user = database.getUser(user_id)
    user["game_data"].pop(game)
    for i in game_data_collector.game_const.gamedata.GameChilds.getChilds(game):
        try:
            user["game_data"].pop(i)
        except KeyError:
            pass
    database.save_user(user, user_id)
    return {"success":True, "message":"Your gameaccount is now unlinked"} 

@app.post("/gamedata/{user_id}")
def getGameData(user_id:str, gameIDsDict: Annotated[str, Form()], request:Request, q : Union[str, None]=None):
    gameIDsDictPair = [i.split(":") for i in gameIDsDict.replace("}", "").replace("{", "").replace("\"", "").split(",")]
    if gameIDsDictPair is None or gameIDsDictPair == []: return
    output = [game_data_collector.get_data(i[0], i[1]) for i in gameIDsDictPair]
    return output

@app.get("/gamedata/game/{game_internal_name}")
def getGameData(game_internal_name:str, request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    user_id = database.get_current_user(request)
    ingame_user_id = database.getUser(user_id)["game_data"][game_internal_name]
    output = game_data_collector.get_data(game_internal_name, ingame_user_id)
    return {"success":True, "body":output, "message":""}

def removeIfExists(string:str, toRemove:str) -> str:
    if toRemove.count(string)>0:
        return string.replace(toRemove, "")
    
# safety
    
def is_safe_path(basedir, path, follow_symlinks=True):
    # resolves symbolic links
    if follow_symlinks:
        matchpath = os.path.realpath(path)
    else:
        matchpath = os.path.abspath(path)
    return basedir == os.path.commonpath((basedir, matchpath))


# captcha

def assess_captcha(token) -> bool:  # currently deactivated

    return True

    threshold = 0.3
    recaptcha_action = "login"
    
    # Set the properties of the event to be tracked.
    event = recaptchaenterprise_v1.Event()
    event.site_key = recaptcha_site_key
    event.token = token

    assessment = recaptchaenterprise_v1.Assessment()
    assessment.event = event

    project_name = f"projects/{project_id}"

    # Build the assessment request.
    request = recaptchaenterprise_v1.CreateAssessmentRequest()
    request.assessment = assessment
    request.parent = project_name

    response = client.create_assessment(request)

    # Check if the token is valid.
    if not response.token_properties.valid:
        print(
            "The CreateAssessment call failed because the token was "
            + "invalid for for the following reasons: "
            + str(response.token_properties.invalid_reason)
        )
        return

    # Check if the expected action was executed.
    if response.token_properties.action != recaptcha_action:
        print(
            "The action attribute in your reCAPTCHA tag does"
            + "not match the action you are expecting to score"
        )
        return
    else:
        # Get the risk score and the reason(s)
        # For more information on interpreting the assessment,
        # see: https://cloud.google.com/recaptcha-enterprise/docs/interpret-assessment
        for reason in response.risk_analysis.reasons:
            print(reason)
        print(
            "The reCAPTCHA score for this token is: "
            + str(response.risk_analysis.score)
        )
        # Get the assessment name (id). Use this to annotate the assessment.
        assessment_name = client.parse_assessment_path(response.name).get("assessment")
        print(f"Assessment name: {assessment_name}")
    return response.risk_analysis.score > threshold

@app.get("/parties/{game}")
def getParties(game:str, request:Request, q : Union[str, None]=None) -> []:
    return database.getParties(game)

@app.get("/parties")
async def getParties(request:Request, q : Union[str, None]=None) -> []:
    parties = database.getParties(None)

    # gather additional info
    for party in parties:
        party_leader_id:str = party["party_leader"]
        party_members_id:[] = party["party_members"]

        party_members = [database.getUser(i) for i in party_members_id]
        party_member_profile_pictures = ["https://"+i["profile_info"]["profile_picture"] for i in party_members]

        preview_profile_pictures = party_member_profile_pictures[0:3]

        try:
            average_rank = 0
            c = 0
            rank_icon = ""
            for i in party_members:
                game_user_id = i["game_data"][party["game"]]
                if game_user_id is None:
                    continue
                game_data = game_data_collector.get_data(party["game"], game_user_id)
                if "mmr" not in game_data["optionalData"].keys():
                    continue
                if game_data["optionalData"]["mmr"].isdigit():
                    average_rank += int(game_data["optionalData"]["mmr"])
                if c == 0 and "rank" in game_data["optionalData"].keys():
                    rank_icon = game_data["optionalData"]["rank"]
                    c += 1
            if c==0:  
                average_rank = -1
            else:
                average_rank = average_rank/c  # maybe also get rank icon???
            if average_rank == 0:
                average_rank = "? "
        except KeyError:
            pass

        party["additional_info"] = {
            "average_mmr" : average_rank,
            "preview_profile_pictures" : preview_profile_pictures,
            "rank_icon": rank_icon
        }

    return parties

@app.post("/create-party")
async def createParty(game: Annotated[str, Form()],
    description: Annotated[str, Form()],
    max_members: Annotated[str, Form()],
    joining_method: Annotated[str, Form()],  # open | only_with_invite
    request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    user_id = database.get_current_user(request)
    party_id = await database.create_party(game, description, max_members, user_id, joining_method=="open")
    return {"success":True, "message":"party created", "party_id":party_id} 

@app.post("/update-party-settings/{party_id}")
async def updateJoiningMethod(
    party_id: str,
    request:Request, q : Union[str, None]=None
):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    changes = await request.json()
    user_id = database.get_current_user(request)
    database.change_party_settings(party_id, changes, user_id)
    return {"success":True, "message":"party setting updated"} 

@app.get("/join-party/{party_id}")
async def joinParty(party_id:int, request:Request, q : Union[str, None]=None) -> int:
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    response_number = await database.join_party(party_id, database.get_current_user(request))
    return response_number

@app.get("/inparty")
def inParty(request:Request, q : Union[str, None]=None) -> str | None:  # None when not in party
    return database.UserInParty(database.get_current_user(request))

@app.get("/party/{party_id}")
def getParty(party_id:int, request:Request, q : Union[str, None]=None):
    user_id = database.get_current_user(request)
    party = database.getParty(party_id)

    # gather additional info
    party_leader_id:str = party["party_leader"]
    party_members_id:List = party["party_members"]
    party_leader_index:int = party_members_id.index(party_leader_id)  # always 0 ?

    party_members = [database.read_user(i, safe=True) for i in party_members_id]
    party["party_member_objects"] = party_members
    party["party_leader_index"] = party_leader_index

    party["messages"] = database.get_party_messages(party_id, user_id)
    return party

@app.get("/kick-party-member/{party_id}/{user_id}")
async def kickPartyMember(party_id:int,user_id:str,request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    return {"success":await database.kick_party_member(party_id, user_id, database.get_current_user(request)), "message":""}

@app.get("/leave-party/{party_id}")
async def leaveParty(party_id:int, request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    return {"success":await database.leave_party(party_id, database.get_current_user(request)), "message":""}

@app.get("/delete-party/{party_id}")
async def deleteParty(party_id:int, request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    return {"success":await database.delete_party(party_id, database.get_current_user(request)), "message":""}


@app.post("/send-party-message/{party_id}")
async def sendPartyMessage(text: Annotated[str, Form()], party_id:int, request:Request, q : Union[str, None]=None):
    if check_login.test(request, database): return {"success":False, "message":"User is not logged in"} 
    return {"success":database.send_party_message(party_id, database.get_current_user(request), text), "message":""}


@app.websocket("/ws")
async def websocket_endpoint(websocket: WebSocket):
    user_id = await database.manager.connect(websocket)
    if user_id is None: return
    try:
        while True:
            data = await websocket.receive()
    except (WebSocketDisconnect, RuntimeError, ConnectionAbortedError, ConnectionResetError):
        database.manager.disconnect(user_id)
    
# ticks every minute
"""@timer_decorator.schedule(60)
def tick():
    print("tick")

tick_thread = Thread(target=tick)
tick_thread.start()"""

if __name__=="__main__":
    uvicorn.run(app, port=8000)
