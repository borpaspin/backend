import hashlib

def salted_hash(password:str):
    return hashlib.sha256((password+"saltt").encode('ansi')).hexdigest()
