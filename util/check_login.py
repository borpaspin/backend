from sqlalchemy.orm import Session

def test(request, database) -> bool:  # True when not logged in
    try:
        login_session_id_cookie = request.headers["login_session_id"]
    except Exception as e:
        return True
    with Session(database.engine) as s:
        if s.query(database.loginSessionToUser).filter_by(login_session_id=login_session_id_cookie).scalar() is not None:
            return False
    return True
