class DoubleSidedDict:
    internal_list1 = []
    internal_list2 = []
    def add(self, value1, value2):
        if self.internal_list1.count(value1)!=0:  # overwrite values, when present
            index = self.internal_list1.index(value1)
            self.internal_list1[index] = value1
            self.internal_list2[index] = value2
            return
        """if self.internal_list2.count(value2)!=0:
            index = self.internal_list2.index(value2)
            self.internal_list1[index] = value1
            self.internal_list2[index] = value2
            return"""
        self.internal_list1.append(value1)
        self.internal_list2.append(value2)
    
    def get1(self, value1):
        index = self.internal_list1.index(value1)
        return self.internal_list2[index]
    
    def get2(self, value2):
        index = self.internal_list2.index(value2)
        return self.internal_list1[index]
    
    def pop1(self, value1):
        index = self.internal_list1.index(value1)
        self.internal_list1.pop(index)
        self.internal_list2.pop(index)

    def pop2(self, value2):
        index = self.internal_list2.index(value2)
        self.internal_list1.pop(index)
        self.internal_list2.pop(index)

    def getList1(self):
        return self.internal_list1
    
    def getList2(self):
        return self.internal_list2
