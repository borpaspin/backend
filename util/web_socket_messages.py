class WebSocketMessage:
    receivers:str # useless value, only for visualization
    type:str = "" # most important value
    body:{} = {}

    def __init__(self):
        pass

    def to_dict(self):
        return {
            "type": self.type,
            "body": self.body
        }
    
class NewPartyMember(WebSocketMessage):  # gets called when other user joins party
    receivers = "party_members"
    type = "NewPartyMember"

    def __init__(self, newPartyMemberID:str):
        self.body = {
            "user_id": newPartyMemberID
        }

class PartyRequestAccepted(WebSocketMessage):  # gets called when user gets into party
    receivers = "new_party_member"
    type = "PartyRequestAccepted"

    def __init__(self, party_id:int):
        self.body = {
            "party_id": party_id
        }

class BroadcastPartyChatMessage(WebSocketMessage):  # gets called to send party message to party members
    pass

class BroadcastPartyChatMessageDeletionMessage(WebSocketMessage):  # gets called to remove party message to party members
    pass

class DeletePartyMessage(WebSocketMessage):  # gets called when Party is deleted
    pass

class PlayerPartyRemovedMessage(WebSocketMessage):  # gets called when User is removed from Party
    pass
