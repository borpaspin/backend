alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
foreign_alphabet = "äüöÄÜÖß"
numbers = "123456789"
common_chars = " _-,."
special_chars = "!*:;|/\\$%&§#'~+@?"

name = alphabet + foreign_alphabet + numbers + common_chars + special_chars

def removeBadChars(string, allowed_characters, questionmarkForFalseChar=True):
    out_str = ""
    c = 0
    for i in (x not in allowed_characters for x in string):
        if not i:
            out_str += string[c]
        elif questionmarkForFalseChar:
            out_str += "?"
        c += 1
    return out_str

